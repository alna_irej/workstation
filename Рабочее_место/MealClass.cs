﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Рабочее_место
{
    class MealClass
    {
        public string name { get; set; }
        public int count { get; set; }

        public MealClass()
        {
            name = "";
            count = 0;
        }
        public MealClass(string name, int count)
        {
            this.name = name;
            this.count = count;
        }
    }

}
