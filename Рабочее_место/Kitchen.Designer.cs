﻿namespace Рабочее_место
{
    partial class Kitchen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Kitchen));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.включитьРежимОфициантToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMsgs = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.txtSend = new System.Windows.Forms.TextBox();
            this.lstClients = new System.Windows.Forms.ListBox();
            this.запуститьСерврToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьПапкуПрограммыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.настройкиToolStripMenuItem,
            this.запуститьСерврToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(595, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.включитьРежимОфициантToolStripMenuItem,
            this.открытьПапкуПрограммыToolStripMenuItem});
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            // 
            // включитьРежимОфициантToolStripMenuItem
            // 
            this.включитьРежимОфициантToolStripMenuItem.Name = "включитьРежимОфициантToolStripMenuItem";
            this.включитьРежимОфициантToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.включитьРежимОфициантToolStripMenuItem.Text = "Включить режим \"Официант\"";
            this.включитьРежимОфициантToolStripMenuItem.Click += new System.EventHandler(this.включитьРежимОфициантToolStripMenuItem_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(345, 24);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(238, 24);
            this.btnLogin.TabIndex = 15;
            this.btnLogin.Text = "Авторизоваться";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click_1);
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(75, 27);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(264, 20);
            this.txtUserName.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Имя повара";
            // 
            // txtMsgs
            // 
            this.txtMsgs.Font = new System.Drawing.Font("GOST type B", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtMsgs.Location = new System.Drawing.Point(6, 53);
            this.txtMsgs.Multiline = true;
            this.txtMsgs.Name = "txtMsgs";
            this.txtMsgs.Size = new System.Drawing.Size(333, 199);
            this.txtMsgs.TabIndex = 12;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(6, 256);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(83, 53);
            this.btnSend.TabIndex = 11;
            this.btnSend.Text = "Отправить";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click_1);
            // 
            // txtSend
            // 
            this.txtSend.Font = new System.Drawing.Font("GOST type B", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtSend.Location = new System.Drawing.Point(89, 258);
            this.txtSend.Multiline = true;
            this.txtSend.Name = "txtSend";
            this.txtSend.Size = new System.Drawing.Size(494, 51);
            this.txtSend.TabIndex = 10;
            // 
            // lstClients
            // 
            this.lstClients.Font = new System.Drawing.Font("GOST type B", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lstClients.FormattingEnabled = true;
            this.lstClients.ItemHeight = 17;
            this.lstClients.Location = new System.Drawing.Point(345, 53);
            this.lstClients.Name = "lstClients";
            this.lstClients.Size = new System.Drawing.Size(238, 191);
            this.lstClients.TabIndex = 9;
            // 
            // запуститьСерврToolStripMenuItem
            // 
            this.запуститьСерврToolStripMenuItem.Name = "запуститьСерврToolStripMenuItem";
            this.запуститьСерврToolStripMenuItem.Size = new System.Drawing.Size(117, 20);
            this.запуститьСерврToolStripMenuItem.Text = "Запустить Сервер";
            this.запуститьСерврToolStripMenuItem.Click += new System.EventHandler(this.запуститьСерврToolStripMenuItem_Click);
            // 
            // открытьПапкуПрограммыToolStripMenuItem
            // 
            this.открытьПапкуПрограммыToolStripMenuItem.Name = "открытьПапкуПрограммыToolStripMenuItem";
            this.открытьПапкуПрограммыToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.открытьПапкуПрограммыToolStripMenuItem.Text = "Открыть папку программы";
            this.открытьПапкуПрограммыToolStripMenuItem.Click += new System.EventHandler(this.открытьПапкуПрограммыToolStripMenuItem_Click);
            // 
            // Kitchen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 320);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtMsgs);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.txtSend);
            this.Controls.Add(this.lstClients);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Kitchen";
            this.Text = "Kitchen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Kitchen_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Kitchen_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem включитьРежимОфициантToolStripMenuItem;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMsgs;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox txtSend;
        private System.Windows.Forms.ListBox lstClients;
        private System.Windows.Forms.ToolStripMenuItem запуститьСерврToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьПапкуПрограммыToolStripMenuItem;
    }
}