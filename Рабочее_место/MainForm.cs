﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace Рабочее_место
{

    public partial class MainForm : Form
    {
        private bool erCo = false, FileNF=false;
        public MainForm()
        {
            InitializeComponent();
        }
        private void edit_Config(string eCo)
        {
            Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            currentConfig.AppSettings.Settings["IsWaiter"].Value = eCo;
            currentConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (erCo == false)
            {
                CreateConfig("true");
            }
            else
            {
                edit_Config("true");
            }
            if (!File.Exists("Menu.xml"))
            {
                FileNF = true;
                MenuNF();
            }
            if (FileNF)
            {
                OpenFileDialog OPF = new OpenFileDialog();
                OPF.Filter = "Файлы xml|*.xml";
                OPF.Title = "Выберите файл с меню";
                if (OPF.ShowDialog() == DialogResult.OK)
                {
                    MenuFile(OPF.FileName);
                }
            }
            else
            {
                Waiter frm = new Waiter();
                frm.Show();
                Hide();
            }
        }


        private void MenuNF()
        {
            label1.Text = "Файл с меню не найден.\r\n Нажмите на кнопку \"Обзор\" и\r\nвыберите файл с меню";
            label2.Text = "Или вы можете загрузить\r\nстандартный пример меню";
            button1.Text = "Обзор";
            button2.Text = "Загрузить пример";
        }

        private void MenuFile(string MenuPath)
        {
            System.IO.File.Copy(MenuPath, Application.StartupPath+@"\Menu.xml", true);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (File.Exists(Application.ExecutablePath + ".config"))
            {
                string IsWaiter = ConfigurationManager.AppSettings["IsWaiter"];
                Opacity = 0;
                ShowInTaskbar = false;
                if (IsWaiter == "true")
                {
                    Waiter frm = new Waiter();
                    frm.Owner = this;
                    frm.Show();
                    timer1.Start();
                }
                else if (IsWaiter == "false")
                {
                    Kitchen frm = new Kitchen();
                    frm.Owner = this;
                    frm.Show();
                    timer1.Start();
                }
                else
                {
                    timer1.Enabled = false;
                    Opacity = 100;
                    ShowInTaskbar = true;
                    erCo = true;
                }
                
            }
            //Close();
        }
        private void CreateConfig(string Wai)
        {
            if (!File.Exists(Application.ExecutablePath+".config")) //если файла нет
            {
                //создаем его
                var def_config = new StreamWriter(Application.ExecutablePath + ".config", false, Encoding.UTF8);
                def_config.Write(
                    "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n" +
                    "<configuration>\n" +
                    "  <appSettings>\n" +
                    "    <add key=\"IsWaiter\" value=\""+Wai+"\" />\n" +
                    "  </appSettings>\n" +
                    "</configuration>\n");
                def_config.Close();
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            Hide();
            timer1.Stop();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (erCo == false)
            {
                CreateConfig("false");
            }
            else
            {
                edit_Config("false");
            }
            if (!File.Exists("Menu.xml"))
            {
                FileNF = true;
                MenuNF();
            }
            if (FileNF)
            {
                MenuFile(Application.StartupPath + @"\Example\Menu.xml");
                Kitchen frm = new Kitchen();
                frm.Show();
                Hide();
            }
            else
            {
                Kitchen frm = new Kitchen();
                frm.Show();
                Hide();
            }
        }
    }
}
