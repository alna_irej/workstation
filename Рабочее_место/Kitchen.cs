﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using Client.ChatService;
using Client;
using System.Diagnostics;

namespace Рабочее_место
{
    public partial class Kitchen : Form
    {
        ReceiveClient rc = null;
        bool stopkey;
        string myName;
        public Kitchen()
        {
            InitializeComponent();
        }

        private void Kitchen_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();

        }

        private void включитьРежимОфициантToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            currentConfig.AppSettings.Settings["IsWaiter"].Value = "true";
            currentConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show("Изменения будут применены после перезапуска программы\n\nПерезапустить программу?", "Внимание", buttons);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Restart();
            }
            
            }



        


        void txtSend_KeyPress(object sender, KeyPressEventArgs e)
        {
            int keyValue = (int)e.KeyChar;

            if (keyValue == 13)
                SendMessage();

        }


        private void frmClient_Load(object sender, EventArgs e)
        {
            txtMsgs.Enabled = false;
            txtSend.Enabled = false;
            btnSend.Enabled = false;
        }

        void rc_ReceiveMsg(string sender, string msg)
        {
            if (msg.Length > 0)
                txtMsgs.Text += Environment.NewLine + sender + ">" + msg;
        }

        void rc_NewNames(object sender, List<string> names)
        {
            lstClients.Items.Clear();
            foreach (string name in names)
            {
                if (name != myName && Space_remove(name) != "Повар")
                    lstClients.Items.Add(name);
            }
        }


        private void SendMessage()
        {
            if (lstClients.Items.Count != 0)
            {
                txtMsgs.Text += Environment.NewLine + myName + ">" + txtSend.Text;
                if (lstClients.SelectedItems.Count == 0)
                    rc.SendMessage(txtSend.Text, myName, lstClients.Items[0].ToString());
                else
                    if (!string.IsNullOrEmpty(lstClients.SelectedItem.ToString()))
                    rc.SendMessage(txtSend.Text, myName, lstClients.SelectedItem.ToString());

                txtSend.Clear();
            }
        }
        string Space_remove(string m)
        {
            String[] words = m.Split(new char[] { '—' }, StringSplitOptions.RemoveEmptyEntries);
            return words[0];
        }

        private void Kitchen_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (stopkey == true)
                rc.Stop(myName);
        }

        private void запуститьСерврToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(Application.StartupPath + "/Server.exe");
        }

        private void открытьПапкуПрограммыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(new ProcessStartInfo("explorer.exe", Application.StartupPath));
        }

        private void btnLogin_Click_1(object sender, EventArgs e)
        {
            if (txtUserName.Text.Length > 0)
            {
                txtMsgs.Enabled = true;
                txtSend.Enabled = true;
                btnSend.Enabled = true;

                myName = "Повар—" + txtUserName.Text.Trim();

                rc = new ReceiveClient();
                rc.Start(rc, myName);

                rc.NewNames += new GotNames(rc_NewNames);
                rc.ReceiveMsg += new ReceviedMessage(rc_ReceiveMsg);
                stopkey = true;
            }
            else
            {
                txtMsgs.Enabled = false;
                txtSend.Enabled = false;
                btnSend.Enabled = false;
                stopkey = false;
            }
        }

        private void btnSend_Click_1(object sender, EventArgs e)
        {
            SendMessage();
        }
    }

}
