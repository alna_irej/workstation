﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Collections.Specialized;
using System.Configuration;
using Client;
using System.Diagnostics;

namespace Рабочее_место
{
    public partial class Waiter : Form
    {
        bool stopkey=false;
        int countresult = 0;
        ReceiveClient rc = null;
        string myName = "Официант";
        List<Order> orders = new List<Order>();
        List<MealClass> Meal = new List<MealClass>();
        string path_xml_base = @"Menu.xml";
        public Waiter()
        {
            InitializeComponent();
        }
        
        private void Waiter_Load(object sender, EventArgs e)
        {
            TabPage[] MyTP = tabControl1.TabPages.Cast<TabPage>().ToArray();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path_xml_base);
            int i = 0, en = 0;
            foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
            {
                foreach (XmlNode node2 in node.ChildNodes)
                {
                    if (en < 8)
                    {
                        MyTP[en].Text = node2.Attributes["type"].Value;
                        foreach (XmlNode node3 in node2.ChildNodes)
                        {
                            if (i < 8)
                            {
                                switch (en)
                                {
                                    case 0:
                                        listBox1.Items.Add(node3.Attributes["name"].Value+ " — " + node3.ChildNodes[1].Attributes["count"].Value);
                                        break;
                                    case 1:
                                        listBox2.Items.Add(node3.Attributes["name"].Value + " — " + node3.ChildNodes[1].Attributes["count"].Value);
                                        break;
                                    case 2:
                                        listBox3.Items.Add(node3.Attributes["name"].Value + " — " + node3.ChildNodes[1].Attributes["count"].Value);
                                        break;
                                    case 3:
                                        listBox4.Items.Add(node3.Attributes["name"].Value + " — " + node3.ChildNodes[1].Attributes["count"].Value);
                                        break;
                                    case 4:
                                        listBox5.Items.Add(node3.Attributes["name"].Value + " — " + node3.ChildNodes[1].Attributes["count"].Value);
                                        break;
                                    case 5:
                                        listBox6.Items.Add(node3.Attributes["name"].Value + " — " + node3.ChildNodes[1].Attributes["count"].Value);
                                        break;
                                    case 6:
                                        listBox7.Items.Add(node3.Attributes["name"].Value + " — " + node3.ChildNodes[1].Attributes["count"].Value);
                                        break;
                                    case 7:
                                        listBox8.Items.Add(node3.Attributes["name"].Value + " — " + node3.ChildNodes[1].Attributes["count"].Value);
                                        break;
                                }
                            }
                        }
                        en++;
                    }
                }
                i++;
            }
            for (int k = en; k < 8; k++)
            {
                MyTP[k].Parent = null;
            }
        }
        

        string Space_remove(string m,short i)
        {
            try
            {
                String[] words = m.Split(new char[] { '—' }, StringSplitOptions.RemoveEmptyEntries);
                return words[i];
            }
            catch
            {
                m = "NULL";
                return m;
            }
        }
        void AddResult (string m)
        {
            if (m != "NULL")
            {
                bool add_new = true;
                if (listBoxResult.Items.Count != 0)
                {
                    for (int j = 0; j < listBoxResult.Items.Count; j++)
                    {
                        if (m == Space_remove(Convert.ToString(listBoxResult.Items[j]), 0))
                        {
                            listBoxResult.Items[j] = x2(Convert.ToString(listBoxResult.Items[j]), true);
                            add_new = false;
                        }

                    }
                }
                if (add_new)
                {
                    listBoxResult.Items.Add(m);
                }
            }
        }
        string x2(string m,bool plus)
        {
            if (Space_remove(m, 1) != "NULL") {
                if (plus)
                {
                    for (int It = 2; It < 100; It++)
                    {

                        if (Space_remove(m, 0) + "— x" + Convert.ToString(It) == m)
                        {

                            m = Space_remove(m, 0) + "— x" + Convert.ToString(It + 1);

                            break;
                        }
                    }
                }
                else
                {
                    
                    for (int It  = Convert.ToInt32( Space_remove(m,1).Remove(0, 2)); It >= 2; It--)
                    {
                        if (Space_remove(m, 0) + "— x" + Convert.ToString(It) == m&&It>2)
                        {

                            m = Space_remove(m, 0) + "— x" + Convert.ToString(It - 1);

                            break;
                        }
                        else if (Space_remove(m, 1) == " x2")
                        {
                            m = Space_remove(m, 0);
                        }
                    }

                }
            }
            else
            { 
                if (plus)
                    m = m + "— x2";
                else
                    m = "NULL";
            }
            return m;
        }
        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                AddResult(Space_remove(Convert.ToString(listBox1.SelectedItem),0));
                countresult += Convert.ToInt32(Space_remove(Convert.ToString(listBox1.SelectedItem), 1));
                label3.Text = "Заказ на " + Convert.ToString(countresult) + " Рублей";
                short k = 1;
                for (int i = 0; i < Meal.Count; i++)
                {
                    if (Space_remove( Meal[i].name,0) ==Space_remove(Convert.ToString( listBox1.SelectedItem),0))
                    {
                        Meal[i].name=x2(Meal[i].name,true);
                        k = 0;
                    }
                }
                if(k==1)
                Meal.Add(new MealClass(Space_remove(Convert.ToString(listBox1.SelectedItem), 0), Convert.ToInt32(Space_remove(Convert.ToString(listBox1.SelectedItem), 1))));

                //listBoxResult.Items.Add(listBox1.SelectedItem);

            }
            catch
            {
                MessageBox.Show("Ничего не выбрано","Ошибка");
            }
            finally
            {
                listBox1.ClearSelected();
            }
        }

        private void listBox2_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                AddResult(Space_remove(Convert.ToString(listBox2.SelectedItem), 0));
                countresult += Convert.ToInt32(Space_remove(Convert.ToString(listBox2.SelectedItem), 1));
                label3.Text = "Заказ на " + Convert.ToString(countresult) + " Рублей";
                short k = 1;
                for (int i = 0; i < Meal.Count; i++)
                {
                    if (Space_remove(Meal[i].name, 0) == Space_remove(Convert.ToString(listBox2.SelectedItem), 0))
                    {
                        Meal[i].name = x2(Meal[i].name, true);
                        k = 0;
                    }
                }
                if (k == 1)
                    Meal.Add(new MealClass(Space_remove(Convert.ToString(listBox2.SelectedItem), 0), Convert.ToInt32(Space_remove(Convert.ToString(listBox2.SelectedItem), 1))));

            }
            catch
            {
                MessageBox.Show("Ничего не выбрано", "Ошибка");
            }
            finally
            {
                listBox2.ClearSelected();
            }
        }

        private void listBox3_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                AddResult(Space_remove(Convert.ToString(listBox3.SelectedItem), 0));
                countresult += Convert.ToInt32(Space_remove(Convert.ToString(listBox3.SelectedItem), 1));
                label3.Text = "Заказ на " + Convert.ToString(countresult) + " Рублей";
                short k = 1;
                for (int i = 0; i < Meal.Count; i++)
                {
                    if (Space_remove(Meal[i].name, 0) == Space_remove(Convert.ToString(listBox3.SelectedItem), 0))
                    {
                        Meal[i].name = x2(Meal[i].name, true);
                        k = 0;
                    }
                }
                if (k == 1)
                    Meal.Add(new MealClass(Space_remove(Convert.ToString(listBox3.SelectedItem), 0), Convert.ToInt32(Space_remove(Convert.ToString(listBox3.SelectedItem), 1))));

            }
            catch
            {
                MessageBox.Show("Ничего не выбрано", "Ошибка");
            }
            finally
            {
                listBox3.ClearSelected();
            }
        }

        private void listBoxResult_MouseClick(object sender, MouseEventArgs e)
        {
            //listBoxResult.Items.Clear();
            //countresult = 0;
            //label3.Text = "";

            //Meal.Add(new MealClass(Space_remove(Convert.ToString(listBox1.SelectedItem), 0), Convert.ToInt32(Space_remove(Convert.ToString(listBox1.SelectedItem), 1))));
            for (int i = 0; i < Meal.Count; i++)
            {
                if (Space_remove(Meal[i].name,0)==Space_remove(Convert.ToString(listBoxResult.SelectedItem), 0))
                {
                    countresult -= Meal[i].count;
                    if(x2(Meal[i].name,false)!="NULL")
                    {
                        Meal[i].name = x2(Meal[i].name, false);
                    }
                    else

                    Meal.RemoveAt(i);
                }
            }
            if (x2(Convert.ToString(listBoxResult.SelectedItem), false) == "NULL")
            {
                listBoxResult.Items.Remove(listBoxResult.SelectedItem);
            }
            else
            { 
                listBoxResult.Items[listBoxResult.SelectedIndex] = x2(Convert.ToString(listBoxResult.SelectedItem), false);
        }
            label3.Text = "Заказ на " + countresult+" Рублей" ;
        }

        private void listBox4_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                AddResult(Space_remove(Convert.ToString(listBox4.SelectedItem), 0));
                countresult += Convert.ToInt32(Space_remove(Convert.ToString(listBox4.SelectedItem), 1));
                label3.Text = "Заказ на " + Convert.ToString(countresult) + " Рублей";
                short k = 1;
                for (int i = 0; i < Meal.Count; i++)
                {
                    if (Space_remove(Meal[i].name, 0) == Space_remove(Convert.ToString(listBox4.SelectedItem), 0))
                    {
                        Meal[i].name = x2(Meal[i].name, true);
                        k = 0;
                    }
                }
                if (k == 1)
                    Meal.Add(new MealClass(Space_remove(Convert.ToString(listBox4.SelectedItem), 0), Convert.ToInt32(Space_remove(Convert.ToString(listBox4.SelectedItem), 1))));

            }
            catch
            {
                MessageBox.Show("Ничего не выбрано", "Ошибка");
            }
            finally
            {
                listBox4.ClearSelected();
            }
        }

        private void listBox5_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                AddResult(Space_remove(Convert.ToString(listBox5.SelectedItem), 0));
                countresult += Convert.ToInt32(Space_remove(Convert.ToString(listBox5.SelectedItem), 1));
                label3.Text = "Заказ на " + Convert.ToString(countresult) + " Рублей";
                short k = 1;
                for (int i = 0; i < Meal.Count; i++)
                {
                    if (Space_remove(Meal[i].name, 0) == Space_remove(Convert.ToString(listBox5.SelectedItem), 0))
                    {
                        Meal[i].name = x2(Meal[i].name, true);
                        k = 0;
                    }
                }
                if (k == 1)
                    Meal.Add(new MealClass(Space_remove(Convert.ToString(listBox5.SelectedItem), 0), Convert.ToInt32(Space_remove(Convert.ToString(listBox5.SelectedItem), 1))));

            }
            catch
            {
                MessageBox.Show("Ничего не выбрано", "Ошибка");
            }
            finally
            {
                listBox5.ClearSelected();
            }
        }

        private void listBox6_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                AddResult(Space_remove(Convert.ToString(listBox6.SelectedItem), 0));
                countresult += Convert.ToInt32(Space_remove(Convert.ToString(listBox6.SelectedItem), 1));
                label3.Text = "Заказ на " + Convert.ToString(countresult) + " Рублей";
                short k = 1;
                for (int i = 0; i < Meal.Count; i++)
                {
                    if (Space_remove(Meal[i].name, 0) == Space_remove(Convert.ToString(listBox6.SelectedItem), 0))
                    {
                        Meal[i].name = x2(Meal[i].name, true);
                        k = 0;
                    }
                }
                if (k == 1)
                    Meal.Add(new MealClass(Space_remove(Convert.ToString(listBox6.SelectedItem), 0), Convert.ToInt32(Space_remove(Convert.ToString(listBox6.SelectedItem), 1))));

            }
            catch
            {
                MessageBox.Show("Ничего не выбрано", "Ошибка");
            }
            finally
            {
                listBox6.ClearSelected();
            }
        }

        private void listBox7_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                AddResult(Space_remove(Convert.ToString(listBox7.SelectedItem), 0));
                countresult += Convert.ToInt32(Space_remove(Convert.ToString(listBox7.SelectedItem), 1));
                label3.Text = "Заказ на " + Convert.ToString(countresult) + " Рублей";
                short k = 1;
                for (int i = 0; i < Meal.Count; i++)
                {
                    if (Space_remove(Meal[i].name, 0) == Space_remove(Convert.ToString(listBox7.SelectedItem), 0))
                    {
                        Meal[i].name = x2(Meal[i].name, true);
                        k = 0;
                    }
                }
                if (k == 1)
                    Meal.Add(new MealClass(Space_remove(Convert.ToString(listBox7.SelectedItem), 0), Convert.ToInt32(Space_remove(Convert.ToString(listBox7.SelectedItem), 1))));

            }
            catch
            {
                MessageBox.Show("Ничего не выбрано", "Ошибка");
            }
            finally
            {
                listBox7.ClearSelected();
            }
        }

        private void listBox8_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                AddResult(Space_remove(Convert.ToString(listBox8.SelectedItem), 0));
                countresult += Convert.ToInt32(Space_remove(Convert.ToString(listBox8.SelectedItem), 1));
                label3.Text = "Заказ на " + Convert.ToString(countresult) + " Рублей";
                short k = 1;
                for (int i = 0; i < Meal.Count; i++)
                {
                    if (Space_remove(Meal[i].name, 0) == Space_remove(Convert.ToString(listBox8.SelectedItem), 0))
                    {
                        Meal[i].name = x2(Meal[i].name, true);
                        k = 0;
                    }
                }
                if (k == 1)
                    Meal.Add(new MealClass(Space_remove(Convert.ToString(listBox8.SelectedItem), 0), Convert.ToInt32(Space_remove(Convert.ToString(listBox8.SelectedItem), 1))));

            }
            catch
            {
                MessageBox.Show("Ничего не выбрано", "Ошибка");
            }
            finally
            {
                listBox8.ClearSelected();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox9.SelectedItem != null)
            {
                int ord = 1;
                //MessageBox.Show(Convert.ToString(comboBox1.SelectedIndex));
                if (comboBox1.SelectedIndex < 0)
                {
                    MessageBox.Show("Выберите столик", "Ошибка");
                }
                else if (listBoxResult.Items.Count == 0)
                {
                    MessageBox.Show("Пустой заказ", "Ошибка");
                }
                else
                {
                    List<string> Temp = new List<string>();
                    if (orders.Count != 0)
                    {
                        for (int k = 0; k < orders.Count; k++)
                        {
                            if (orders[k].Table == comboBox1.SelectedIndex)
                            {
                                ord *= 0;
                            }
                            else
                            {
                                ord *= 1;
                            }
                        }
                        if (ord == 0)
                        {
                            for (int k = 0; k < orders.Count; k++)
                            {
                                for (int j = 0; j < listBoxResult.Items.Count; j++)
                                {
                                    //WorkStation.MealClass asd = new WorkStation.MealClass(Convert.ToString(listBoxResult.Items[j]),10);
                                    //orders[k].Meal.Add(new WorkStation.MealClass(Convert.ToString(listBoxResult.Items[j]), 10));
                                    //orders[k].Meal[j].name = Convert.ToString(listBoxResult.Items[j]);
                                    //MessageBox.Show(Convert.ToString("1 " + listBoxResult.Items.Count));
                                    orders[k].Meal.Add(Convert.ToString(listBoxResult.Items[j]));
                                    listBoxOrder.Items.Add(listBoxResult.Items[j]);
                                    SendMessage(Convert.ToString(listBoxResult.Items[j]) + " Столик №" + comboBox1.SelectedItem);
                                }
                                break;
                            }
                        }
                        else
                        {
                            for (int j = 0; j < listBoxResult.Items.Count; j++)
                            {
                                //MessageBox.Show(Convert.ToString("2 " + listBoxResult.Items.Count));
                                Temp.Add(Convert.ToString(listBoxResult.Items[j]));
                                listBoxOrder.Items.Add(listBoxResult.Items[j]);
                                SendMessage(Convert.ToString(listBoxResult.Items[j]) + " Столик №" + comboBox1.SelectedItem);
                            }
                            orders.Add(new Order(Temp, Convert.ToString(DateTime.Now.Year) + "-" + Convert.ToString(DateTime.Now.Month) + "-" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Hour) + "-" + Convert.ToString(DateTime.Now.Minute) + "-" + Convert.ToString(DateTime.Now.Second), comboBox1.SelectedIndex,countresult));
                        }

                    }
                    else
                    {
                        for (int j = 0; j < listBoxResult.Items.Count; j++)
                        {
                            //MessageBox.Show(Convert.ToString("3 "+listBoxResult.Items.Count));
                            Temp.Add(Convert.ToString(listBoxResult.Items[j]));
                            listBoxOrder.Items.Add(listBoxResult.Items[j]);
                            SendMessage(Convert.ToString(listBoxResult.Items[j]) + " Столик №" + comboBox1.SelectedItem);
                        }
                        orders.Add(new Order(Temp, Convert.ToString(DateTime.Now.Year) + "-" + Convert.ToString(DateTime.Now.Month) + "-" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Hour) + "-" + Convert.ToString(DateTime.Now.Minute) + "-" + Convert.ToString(DateTime.Now.Second), comboBox1.SelectedIndex,countresult));
                    }
                    listBoxResult.Items.Clear();
                    countresult = 0;
                    label3.Text = "";
                    //MessageBox.Show(Convert.ToString(DateTime.Now.Year)+"-"+ Convert.ToString(DateTime.Now.Month)+ "-" + Convert.ToString(DateTime.Now.Day)+ "_" + Convert.ToString(DateTime.Now.Hour)+ "-" + Convert.ToString(DateTime.Now.Minute)+ "-" + Convert.ToString(DateTime.Now.Second));
                    //SendMessage(orders);
                }

            }
            else
            {
                MessageBox.Show("Выберите повара", "Ошибка");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (orders.Count != 0)
            {
                for (int k = 0; k < orders.Count; k++)
                {
                    if (orders[k].Table == comboBox1.SelectedIndex)
                    {
                        DirectoryInfo dir = new DirectoryInfo(Application.StartupPath + "/Logs/");
                        dir.Create();
                        var def_config = new StreamWriter(Application.StartupPath + "/Logs/"+orders[k].ID, false, Encoding.UTF8);
                        def_config.Write("Столик № "+orders[k].Table + "\n");
                        for (short i = 0; i < orders[k].Meal.Count; i++)
                            def_config.Write(orders[k].Meal[i] + "\n");
                        def_config.Write(orders[k].Count);
                        def_config.Close();
                        orders.RemoveAt(k);

                    }
                }
            }
            
            if (listBoxOrder.Items.Count != 0)
            {
                MessageBox.Show("Заказ на столике №" + comboBox1.SelectedItem + " завершен", "Выполнено");
                listBoxOrder.Items.Clear();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool t = true;
            int i = 0;
            listBoxOrder.Items.Clear();
            for(i=0;i<orders.Count;i++)
            {
                if (orders[i].Table == comboBox1.SelectedIndex)
                {
                    t = true;
                    break;
                }
                else
                    t = false;
            }
            if (orders.Count!=0)
            {
                if (t == true)
                {
                    for (int j = 0; j < orders[i].Meal.Count; j++)
                    {
                        listBoxOrder.Items.Add(orders[i].Meal[j]);
                    }
                    label4.Text = "Заказ на "+Convert.ToString(orders[i].Count)+" Рублей";
                }
            }
           //MessageBox.Show(Convert.ToString(i));
        }

        private void Waiter_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void подменю21ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            currentConfig.AppSettings.Settings["IsWaiter"].Value = "false";
            currentConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show("Изменения будут применены после перезапуска программы\n\nПерезапустить программу?", "Внимание",buttons);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Restart();
            }
        }
        //код для обмена сообщения с кухней



        void rc_ReceiveMsg(string sender, string msg)
        {
            if (msg.Length > 0)
                textBox1.Text += Environment.NewLine + sender + ">" + msg;
        }

        void rc_NewNames(object sender, List<string> names)
        {
            listBox9.Items.Clear();
            foreach (string name in names)
            {
                if (name != myName&&Space_remove(name,0)!="Официант")
                    listBox9.Items.Add(name);
            }
        }


        private void SendMessage(string WaiterMessageText)
        {
            
            if (listBox9.Items.Count != 0)
            {
                textBox1.Text += Environment.NewLine + myName + ">" + listBoxOrder.Text;
                if (listBox9.SelectedItems.Count == 0)
                    rc.SendMessage(WaiterMessageText, myName, listBox9.Items[0].ToString());
                else
                    if (!string.IsNullOrEmpty(listBox9.SelectedItem.ToString()))
                    rc.SendMessage(WaiterMessageText, myName, listBox9.SelectedItem.ToString());

                //listBoxOrder.Clear();
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (txtUserName.Text.Length > 0)
            {
                listBox1.Enabled = true;
                listBox2.Enabled = true;
                listBox3.Enabled = true;
                listBox4.Enabled = true;
                listBox5.Enabled = true;
                listBox6.Enabled = true;
                listBox7.Enabled = true;
                listBox8.Enabled = true;
                button1.Enabled = true;
                button2.Enabled = true;

                myName = "Официант—"+txtUserName.Text.Trim();

                rc = new ReceiveClient();
                rc.Start(rc, myName);

                rc.NewNames += new GotNames(rc_NewNames);
                rc.ReceiveMsg += new ReceviedMessage(rc_ReceiveMsg);
                label2.Visible = false;
                txtUserName.Visible = false;
                button3.Visible = false;
                stopkey = true;
            }
            else
            {
                listBox1.Enabled = false;
                listBox2.Enabled = false;
                listBox3.Enabled = false;
                listBox4.Enabled = false;
                listBox5.Enabled = false;
                listBox6.Enabled = false;
                listBox7.Enabled = false;
                listBox8.Enabled = false;
                button1.Enabled = false;
                button2.Enabled = false;
                stopkey = false;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void Waiter_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(stopkey==true)
            rc.Stop(myName);
        }

        private void запуститьСерверToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Process.Start(Application.StartupPath+"/Server.exe");
        }

        private void открытьПапкуПрограммыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(new ProcessStartInfo("explorer.exe", Application.StartupPath));


        }

        private void txtUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            int keyValue = (int)e.KeyChar;

            if (keyValue == 13)
                button3_Click_1(sender,e);
        }
    }
}
