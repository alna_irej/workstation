﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Рабочее_место
{
    class Order
    {
        public List<string> Meal{ get; set; }
        public string ID { get; set; }
        public int Table { get; set; }
        public int Count { get; set; }
        
        public Order ()
        {
            Meal = new List<string>();
            ID = "";
            Table = new int();
            Count = 0;
        }

        public Order(List<string> OrderMeal, string OrderID, int OrderTable,int OrderCount)
        {
            Meal = OrderMeal;
            ID = OrderID;
            Table = OrderTable;
            Count = OrderCount;
        }
    }
}
